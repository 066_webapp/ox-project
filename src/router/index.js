import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Mypage from '../views/MyPage'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/mypage/:name',
    name: 'MyPage',
    component: Mypage,
    props: true
  },
  {
    path: '/mygame',
    name: 'MyGame',
    component: () => import('../views/MyGame.vue')
    // component: function () {
    //   return import('../views/MyGame.vue')
    // }
  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
